### BitBucket App using MVVM

## Feature

* Splash
* Login
* My repositoires
* Repository Details
* Profile

## Tech Stack

* Kotlin
* RxJava
* Android Architecture Components (LiveData - Viewmodel)
* Koin
* Retrofit
* Room
* GSON
* Glide

## IDE Used

* Android Studio Arctic Fox | 2020.3.1 Patch 2 (4.1.1)


## Screenshots

![Splash](https://i.ibb.co/Tv5fPhw/Screenshot-1631997057.png)
![Login](https://i.ibb.co/brfRrnq/Screenshot-1631997059.png)
![Home](https://i.ibb.co/yRW0kn4/Screenshot-1631995193.png)
![Details](https://i.ibb.co/4JPLPsz/Screenshot-1631997082.png)
![Profile](https://i.ibb.co/MRfxnvR/Screenshot-1631997084.png)
![Logout](https://i.ibb.co/xCC75s5/Screenshot-1631997086.png)

#### Thank you :)