package com.app.mybitbucket.ui.splash.presntation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.app.mybitbucket.R
import com.app.mybitbucket.network.retrofit.UserSessionPref
import com.app.mybitbucket.ui.base.BaseActivity
import com.app.mybitbucket.ui.login.presntation.LoginActivity
import com.app.mybitbucket.ui.main.presntation.MainActivity
import org.koin.android.ext.android.inject

class SplashActivity : BaseActivity() {

    private val userSession: UserSessionPref by inject()


    override fun buildIntent(context: Context, data: Any?): Intent =
        Intent(context, SplashActivity::class.java)

    override fun getActivityView(): Int = R.layout.activity_splash

    override fun afterInflation(savedInstance: Bundle?) {
        Handler().postDelayed({
            if(userSession.getUser()==null){
                startActivity(
                    LoginActivity()
                        .buildIntent(this, null)
                )
            }else{
                startActivity(
                    MainActivity()
                        .buildIntent(this, null)
                )
            }
            finish()
        }, 2000)
    }

}