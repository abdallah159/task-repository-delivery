package com.app.mybitbucket.ui.login.presntation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.app.mybitbucket.R
import com.app.mybitbucket.model.user.UserInfoModel
import com.app.mybitbucket.network.retrofit.UserSessionPref
import com.app.mybitbucket.ui.base.BaseActivity
import com.app.mybitbucket.ui.login.viewmodel.LoginViewModel
import com.app.mybitbucket.ui.main.presntation.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : BaseActivity() {

    var userNameWritten: MutableLiveData<Boolean> = MutableLiveData(false)
    var passwordWritten: MutableLiveData<Boolean> = MutableLiveData(false)

    private val loginVM: LoginViewModel by viewModel()
    private val userSession: UserSessionPref by inject()

    private var authString = ""

    override fun getActivityView(): Int = R.layout.activity_login

    override fun afterInflation(savedInstance: Bundle?) {
        backIV.setOnClickListener { onBackPressed() }
        watchEmailAndPassword()


        loginVM.loading.observe(this, {
            if (it)
                loadingPB.visibility = View.VISIBLE
            else
                loadingPB.visibility = View.GONE
        })

        loginVM.loginError.observe(this, {
            if (it) {
                showSnackMsg(getString(R.string.user_name_or_password_error))
            }
        })

        loginVM.response.observe(this, {
            if (it != null) {
                userSession.saveUserInfo(UserInfoModel(userNameET.text.toString(), authString))
                userSession.saveUserData(it)
                Toast.makeText(this, getString(R.string.login_is_succuss), Toast.LENGTH_SHORT)
                    .show()
                startActivity(MainActivity().buildIntent(this, null))
                this.finishAffinity()
            }
        })

        loginFAB.setOnClickListener {
            if (userNameWritten.value?.equals(true)!! && passwordWritten.value?.equals(true)!!) {
                authString = "Basic ${
                    Base64.encodeToString(
                        ("${userNameET.text.toString()}:${passwordET.text.toString()}").toByteArray(),
                        Base64.NO_WRAP
                    )
                }"
                loginVM.login(
                    authString
                )
            }
        }
    }


    private fun watchEmailAndPassword() {
        userNameET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                userNameWritten.value = !p0.isNullOrEmpty()
            }
        })
        passwordET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                passwordWritten.value = !p0.isNullOrEmpty()
            }
        })
        userNameWritten.observe(this, Observer {
            if (it) {
                passwordWritten.observe(this, Observer { writed ->
                    if (writed) {
                        loginFAB.alpha = 1f
                        loginFAB.isEnabled = true
                    } else {
                        loginFAB.alpha = 0.4f
                        loginFAB.isEnabled = false

                    }
                })
            } else {
                loginFAB.alpha = 0.4f
                loginFAB.isEnabled = false
            }
        })
    }

    override fun buildIntent(context: Context, data: Any?): Intent {
        return Intent(context, LoginActivity::class.java)
    }
}