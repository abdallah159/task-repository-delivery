package com.app.mybitbucket.ui.home.presntation.adapter

import android.widget.TextView
import com.app.mybitbucket.R
import com.app.mybitbucket.model.repo.RepositoryModel
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class RepositoriesAdapter(
    private val items: MutableList<RepositoryModel>,
    val onItemClicked: (Int) -> Unit
) :
    BaseQuickAdapter<RepositoryModel, BaseViewHolder>(R.layout.item_repository, items) {

    override fun convert(helper: BaseViewHolder, item: RepositoryModel) {
        with(helper) {
            getView<TextView>(R.id.nameTV).text = item.name
            if (item.is_private) {
                getView<TextView>(R.id.typeTV).text =
                    "${context.getString(R.string.repo_type)} ${context.getString(R.string.private_repo)}"
            } else {
                getView<TextView>(R.id.typeTV).text =
                    "${context.getString(R.string.repo_type)} ${context.getString(R.string.public_repo)}"
            }
            getView<TextView>(R.id.fullNameTV).text =
                "${context.getString(R.string.full_name)} ${item.full_name}"


            itemView.setOnClickListener {
                onItemClicked(adapterPosition)
            }
        }
    }
}