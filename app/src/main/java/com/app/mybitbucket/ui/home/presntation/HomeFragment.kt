package com.app.mybitbucket.ui.home.presntation

import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.mybitbucket.R
import com.app.mybitbucket.model.repo.RepositoryModel
import com.app.mybitbucket.network.retrofit.UserSessionPref
import com.app.mybitbucket.ui.base.BaseFragment
import com.app.mybitbucket.ui.home.presntation.adapter.RepositoriesAdapter
import com.app.mybitbucket.ui.home.viewmodel.HomeViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment() {


    lateinit var repositoriesAdapter: RepositoriesAdapter
    lateinit var repositoriesLayoutManager: LinearLayoutManager


    private val homeViewModel: HomeViewModel by viewModel()
    private val userSession: UserSessionPref by inject()

    override fun getFragmentView(): Int = R.layout.fragment_home

    override fun setUp(view: View?) {
        setUpRepositoriesList()

        homeViewModel.reposError.observe(this, {
            if (it) {
                showMessage(getString(R.string.general_error))
            }
        })

        homeViewModel.loading.observe(this, {
            if (it)
                showLoading()
            else
                hideLoading()
        })

        userSession.getUser()?.let {
            homeViewModel.getUserRepositories(
                it.userEncryptedAuth,
                it.userName
            )
        }

        homeViewModel.response.observe(this, Observer {
            repositoriesAdapter.data = it
            repositoriesAdapter.notifyDataSetChanged()
        })

    }


    private fun setUpRepositoriesList() {
        repositoriesAdapter = RepositoriesAdapter(mutableListOf()) {
            showRepoDetails(homeViewModel.response.value!![it])
        }
        repositoriesLayoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        repositoriesRV.apply {
            layoutManager = repositoriesLayoutManager
            adapter = repositoriesAdapter
        }
    }

    private fun showRepoDetails(repo: RepositoryModel) {
        val inflatedViewTypes: View = layoutInflater.inflate(R.layout.item_repository_details, null)
        var repoDetailsDialog: BottomSheetDialog =
            BottomSheetDialog(requireContext())
        repoDetailsDialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        repoDetailsDialog.setCancelable(true)
        repoDetailsDialog.setContentView(inflatedViewTypes)

        var nameTV = inflatedViewTypes.findViewById<TextView>(R.id.nameTV)
        var descTV = inflatedViewTypes.findViewById<TextView>(R.id.descTV)
        var typeTV = inflatedViewTypes.findViewById<TextView>(R.id.typeTV)
        var fullNameTV = inflatedViewTypes.findViewById<TextView>(R.id.fullNameTV)
        var linkTV = inflatedViewTypes.findViewById<TextView>(R.id.linkTV)

        nameTV.text = repo.name
        descTV.text = repo.description

        fullNameTV.text = "${requireContext().getString(R.string.full_name)} ${repo.full_name}"
        if (repo.is_private) {
            typeTV.text =
                "${requireContext().getString(R.string.repo_type)} ${requireContext().getString(R.string.private_repo)}"
        } else {
            typeTV.text =
                "${requireContext().getString(R.string.repo_type)} ${requireContext().getString(R.string.public_repo)}"
        }

        linkTV.setOnClickListener {
            var linkIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse(repo.links.self.href))
            startActivity(linkIntent)
        }

        repoDetailsDialog.show()
    }


    companion object {
        fun newInstance() = HomeFragment()
    }
}