package com.app.mybitbucket.ui.base

interface DialogViewCallBack : BaseViewCallBack {

    fun dismissDialog(tag: String)
}
