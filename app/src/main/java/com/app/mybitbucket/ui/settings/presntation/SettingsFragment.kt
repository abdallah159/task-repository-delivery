package com.app.mybitbucket.ui.settings.presntation

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.app.mybitbucket.R
import com.app.mybitbucket.network.retrofit.UserSessionPref
import com.app.mybitbucket.ui.base.BaseFragment
import com.app.mybitbucket.ui.login.presntation.LoginActivity
import com.app.mybitbucket.utils.loadImage
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.IO
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class SettingsFragment : BaseFragment() {


    private val userSession: UserSessionPref by inject()


    override fun getFragmentView(): Int = R.layout.fragment_settings

    override fun setUp(view: View?) {
        userSession.getUserData()?.let {
            profileIV.loadImage(it.links.avatar.href)
            nameTV.text = it.display_name
            userNameTV.text = it.username
            userIdTV.text = it.account_id

            profileLinkTV.setOnClickListener { view ->
                var linkIntent =
                    Intent(Intent.ACTION_VIEW, Uri.parse(it.links.html.href))
                startActivity(linkIntent)
            }
        }

        logoutCV.setOnClickListener {
            var builder = AlertDialog.Builder(requireContext(), R.style.AlertDialogTheme)
            builder.setTitle(getString(R.string.confirm_logout))
            builder.setMessage(getString(R.string.logout_confirmation_message))
            builder.setPositiveButton(getString(R.string.yes)) { dialog, _ ->
                userSession.logout()
                startActivity(LoginActivity().buildIntent(requireContext(), null))
                activity?.finishAffinity()
                dialog.cancel()
            }
            builder.setNegativeButton(getString(R.string.no)) { dialog, _ ->
                dialog.cancel()
            }
            var alert = builder.create()
            alert.show()
        }
    }


    companion object {
        fun newInstance() = SettingsFragment()
    }
}