package com.app.mybitbucket.ui.main.presntation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.app.mybitbucket.R
import com.app.mybitbucket.ui.base.BaseActivity
import com.app.mybitbucket.ui.home.presntation.HomeFragment
import com.app.mybitbucket.ui.settings.presntation.SettingsFragment
import com.app.mybitbucket.utils.getColorFromAttr
import com.app.mybitbucket.utils.showAndHideFragmentsToActivity
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), AHBottomNavigation.OnTabSelectedListener {

    private var mainPageFragments: MutableList<Fragment> = mutableListOf()
    lateinit var selectedFragment: Fragment


    override fun getActivityView(): Int = R.layout.activity_main

    override fun afterInflation(savedInstance: Bundle?) {
        setupViewPager()
    }

    private fun setupViewPager() {

        mainPageFragments.add(HomeFragment.newInstance())
        mainPageFragments.add(SettingsFragment.newInstance())

        supportFragmentManager.beginTransaction().add(R.id.container, mainPageFragments[0], "0")
            .commit()
        supportFragmentManager.beginTransaction().show(mainPageFragments[0]).commit()
        selectedFragment = mainPageFragments[0]


        val navigationAdapter = AHBottomNavigationAdapter(this, R.menu.bottom_navigation_menu)

        navigationAdapter.setupWithBottomNavigation(bottomNavigation)
        bottomNavigation.accentColor = getColorFromAttr(R.attr.colorPrimary)
        bottomNavigation.titleState = AHBottomNavigation.TitleState.ALWAYS_SHOW
        bottomNavigation.setOnTabSelectedListener(this)
        bottomNavigation.setTitleTextSizeInSp(14f, 14f)
    }

    override fun onTabSelected(position: Int, wasSelected: Boolean): Boolean {
        showAndHideFragmentsToActivity(
            active = selectedFragment,
            newFragment = mainPageFragments[position]
        )
        selectedFragment = mainPageFragments[position]
        return true
    }

    override fun onBackPressed() {
        if (selectedFragment != mainPageFragments[0]) {
            bottomNavigation.currentItem = 0
            selectedFragment = mainPageFragments[0]
        } else {
            super.onBackPressed()
        }
    }

    override fun buildIntent(context: Context, data: Any?): Intent {
        return Intent(context, MainActivity::class.java)
    }


}