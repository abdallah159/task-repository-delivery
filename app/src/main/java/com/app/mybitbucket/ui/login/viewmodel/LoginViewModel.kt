package com.app.mybitbucket.ui.login.viewmodel

import android.util.Base64
import androidx.lifecycle.MutableLiveData
import com.app.mybitbucket.model.user.UserModel
import com.app.mybitbucket.network.AppRepository
import com.app.mybitbucket.network.retrofit.CallbackWrapper
import com.app.mybitbucket.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginViewModel(private val repository: AppRepository) : BaseViewModel() {

    var response: MutableLiveData<UserModel> = MutableLiveData()
    var loginError: MutableLiveData<Boolean> = MutableLiveData(false)

    fun login(userAuth: String) {
        loading.value = true

        mCompositeDisposable.add(
            repository.getUserDetails(userAuth)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : CallbackWrapper<UserModel>() {

                    override fun onSuccess(t: UserModel) {
                        loading.value = false
                        loginError.value = false
                        response.postValue(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        loginError.value = true
                        loading.value = false
                    }

                    override fun onFail(t: String?) {

                    }

                })
        )
    }


}