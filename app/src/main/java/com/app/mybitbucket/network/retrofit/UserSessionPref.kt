package  com.app.mybitbucket.network.retrofit

import android.content.Context
import android.content.SharedPreferences
import com.app.mybitbucket.model.user.UserInfoModel
import com.app.mybitbucket.model.user.UserModel
import com.google.gson.Gson

/**
 * LoginPref used as SharedPreferences holder among all the application
 * @author Abdallah Mohamed
 * @param context is application context
 */
class UserSessionPref(context: Context) {

    private var prefs: SharedPreferences? = null

    init {
        try {
            prefs =
                context.applicationContext.getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE)
        } catch (e: Exception) {
        }
    }

    /**
     * logout used to clear user data from pref
     * @author Abdallah Mohamed
     */
    fun logout() {
        prefs?.edit()?.clear()?.apply()
    }

    companion object {
        private val LOGIN_PREF = "LOGIN_PREF"
    }


    /**
     * saveUser used to save user in pref
     * @author Abdallah Mohamed
     * @param user User data saved in UserModel class from api response
     */
    fun saveUserInfo(user: UserInfoModel) {
        val gson = Gson()
        val json = gson.toJson(user)
        prefs?.edit()?.putString("user", json)?.apply()
    }

    fun getUser(): UserInfoModel? {
        val gson = Gson()
        val json = prefs!!.getString("user", null)
        return gson.fromJson(json, UserInfoModel::class.java)
    }


    /**
     * saveUser used to save user in pref
     * @author Abdallah Mohamed
     * @param user User data saved in UserModel class from api response
     */
    fun saveUserData(user: UserModel) {
        val gson = Gson()
        val json = gson.toJson(user)
        prefs?.edit()?.putString("user_data", json)?.apply()
    }

    fun getUserData(): UserModel? {
        val gson = Gson()
        val json = prefs!!.getString("user_data", null)
        return gson.fromJson(json, UserModel::class.java)
    }

}
