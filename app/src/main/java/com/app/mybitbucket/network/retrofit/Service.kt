package  com.app.mybitbucket.network.retrofit

import com.app.mybitbucket.model.GenericResponse
import com.app.mybitbucket.model.repo.RepositoryModel
import com.app.mybitbucket.model.user.UserModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path


interface Service {

    @GET("user")
    fun getUserDetails(
        @Header("Authorization") authorization: String
    ): Observable<UserModel>

    @GET("users/{userName}/repositories")
    fun getUserRepositories(
        @Header("Authorization") authorization: String,
        @Path("userName") userName: String
    ): Observable<GenericResponse<MutableList<RepositoryModel>>>


}