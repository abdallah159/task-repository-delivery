package  com.app.mybitbucket.network


import com.app.mybitbucket.App
import com.app.mybitbucket.model.GenericResponse
import com.app.mybitbucket.model.user.UserModel
import com.app.mybitbucket.model.repo.RepositoryModel
import com.app.mybitbucket.network.retrofit.Service
import io.reactivex.Observable

class AppRepository : Service {
    override fun getUserDetails(authorization: String): Observable<UserModel> {
        return App.getService.getUserDetails(authorization)
    }

    override fun getUserRepositories(
        authorization: String,
        userName: String
    ): Observable<GenericResponse<MutableList<RepositoryModel>>> {
        return App.getService.getUserRepositories(authorization,userName)
    }

}
