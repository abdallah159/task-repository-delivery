package com.app.mybitbucket

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.androidnetworking.interceptors.HttpLoggingInterceptor
import com.app.mybitbucket.di.appModule
import com.app.mybitbucket.network.retrofit.APInterceptor
import com.app.mybitbucket.network.retrofit.Service
import com.google.android.gms.security.ProviderInstaller
import okhttp3.*
import org.koin.android.ext.android.startKoin
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class App : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        createApi(APInterceptor(), this)
        startKoin(this, listOf(appModule))
        MultiDex.install(this)
    }


    companion object {
        lateinit var getService: Service

        internal fun createApi(
            apiInterceptor: APInterceptor?,
            context: Context
        ) {
            val clientBuilder: OkHttpClient.Builder
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            clientBuilder = OkHttpClient.Builder()
                .addInterceptor(interceptor)

            apiInterceptor?.let {
                clientBuilder.addInterceptor(
                    apiInterceptor
                )
            }



            ProviderInstaller.installIfNeeded(context)
            val client: OkHttpClient = clientBuilder.build()


            val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            getService = retrofit.create(Service::class.java)
        }
    }
}
