package com.app.mybitbucket.di

import com.app.mybitbucket.network.AppRepository
import com.app.mybitbucket.network.retrofit.UserSessionPref
import com.app.mybitbucket.ui.home.viewmodel.HomeViewModel
import com.app.mybitbucket.ui.login.viewmodel.LoginViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val appModule = module {

    //repo
    factory { AppRepository() }
    factory { UserSessionPref(get()) }
    //view models
    viewModel { LoginViewModel(get()) }
    viewModel { HomeViewModel(get()) }

}