package com.app.mybitbucket.model.repo

data class Workspace(
    val name: String,
    val slug: String,
    val type: String,
    val uuid: String
)