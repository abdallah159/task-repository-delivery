package com.app.mybitbucket.model.repo

data class Owner(
    val account_id: String,
    val display_name: String,
    val links: Links,
    val nickname: String,
    val type: String,
    val uuid: String
)