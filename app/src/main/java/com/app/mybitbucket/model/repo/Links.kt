package com.app.mybitbucket.model.repo

data class Links(
    val avatar: Avatar,
    val self: Avatar,
    val html: Avatar
)