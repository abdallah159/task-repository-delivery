package com.app.mybitbucket.model.repo

data class Source(
    val href: String
)