package com.app.mybitbucket.model

data class GenericResponse<T>(
    val message: String?,
    val values: T?,
    val pagelen: Int? = 0,
    val page: Int? = 0,
    val size: Int? = 0
)