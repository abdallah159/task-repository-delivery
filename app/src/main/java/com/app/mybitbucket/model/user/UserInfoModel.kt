package com.app.mybitbucket.model.user

data class UserInfoModel(
    val userName: String,
    val userEncryptedAuth: String
)
