package com.app.mybitbucket.model.repo

data class Mainbranch(
    val name: String,
    val type: String
)