package com.app.mybitbucket.model.repo

data class Project(
    val key: String,
    val name: String,
    val type: String,
    val uuid: String
)