package com.app.mybitbucket.model.repo

data class Avatar(
    val href: String
)