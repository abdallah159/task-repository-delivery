package com.app.mybitbucket.model.user

import com.app.mybitbucket.model.repo.Links

data class UserModel(
    val account_id: String,
    val account_status: String,
    val created_on: String,
    val display_name: String,
    val has_2fa_enabled: Any?,
    val is_staff: Boolean,
    val links: Links,
    val location: Any?,
    val nickname: String,
    val type: String,
    val username: String,
    val uuid: String
)